"""
Routes and views for the flask application.
"""
import socket
from datetime import datetime
from flask import render_template, Response
from ProjetWeb import app
import mysql.connector as mysql

ON = True

host = '0.0.0.0'
port = 10001
client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

class Database:
    def __init__(self):
        db = "test"
        self.con = mysql.connect(db=db)
        self.cur = self.con.cursor()

    def information(self):
        try:
            self.cur.execute("SELECT * FROM profil")
            result = self.cur.fetchall()
        except Exception as e:
            print(e)
        return result

    def list_pomp(self):
        result = ()
        try:
            self.cur.execute("SELECT * FROM station")
            rows = self.cur.fetchall()
            for row in rows:
                result += ((row[0], row[1],'{:0.2f}'.format(row[2]),row[3]),)
        except Exception as e:
            print(e)
        return result

@app.route('/')
@app.route('/home')
def home():
    """Renders the home page."""
    global ON
    return render_template(
        'index.html',
        title='Home Page',
        ON = ON,
        year=datetime.now().year,
    )

@app.route('/contact')
def contact():
    db = Database()
    info = db.information()[0]
    """Renders the contact page."""
    return render_template(
        'contact.html',
        title='Contact',
        year=datetime.now().year,
        message='Your contact page.',
        address= info[1],
        phone= info[2]
    )

@app.route('/about')
def about():
    """Renders the about page."""
    return render_template(
        'about.html',
        title='About',
        year=datetime.now().year,
        message='Your application description page.'
    )

def reponse():
    return render_template(
        'reponse.html',
        title = 'Reponse',
        year=datetime.now().year,
        message='Your application description page.'
    )

@app.route('/reponse')
def station():
    def db_query():
        db = Database()
        emps = db.list_pomp()
        return emps
    res = db_query()
    return render_template(
        'reponse.html', 
        title = 'Reponse',
        year=datetime.now().year,
        result=res,
        content_type='application/json'
        )

@app.route('/start')
def start():
    client.connect((host,port))
    client.send(b'\x01I40100')
    global ON
    ON = False
    return '', 204

@app.route('/stop')
def stop():
    client.send(b'\x01I40200')
    client.close()
    global ON
    ON = True
    return '', 204

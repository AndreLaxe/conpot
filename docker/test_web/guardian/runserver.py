"""
This script runs the ProjetWeb application using a development server.
"""

from os import environ
from ProjetWeb import app

if __name__ == '__main__':
    HOST = environ.get('SERVER_HOST', '0.0.0.0')
    try:
        PORT = int(environ.get('SERVER_PORT', '8800'))
    except ValueError:
        PORT = 8800
    app.run(HOST,PORT)

"""
Routes and views for the flask application.
"""

import socket
import codecs
from datetime import datetime
from flask import Flask, render_template, request
from WebSiteDNP3 import app
from WebSiteDNP3.dnp3mod import sendValue

dic = {'Turbin': 0,
       'Door': 1}

host = '0.0.0.0'
port = 20000
server = (host, port)

@app.route('/')
@app.route('/home')
def home():
    """Renders the home page."""
    return render_template(
        'index.html',
        title='Home Page',
        year=datetime.now().year,
    )

@app.route('/contact')
def contact():
    """Renders the contact page."""
    return render_template(
        'contact.html',
        title='Contact',
        year=datetime.now().year,
        message='Your contact page.'
    )

@app.route('/about')
def about():
    """Renders the about page."""
    return render_template(
        'about.html',
        title='About',
        year=datetime.now().year,
        message='Your application description page.'
    )

@app.route('/plan')
def station():
    return render_template(
        'station.html',
        title = 'Plan',
        yes = datetime.now().year,
        message = 'Plan station',
        speed = 100,
        checked = True
        )


@app.route('/test', methods=['POST'])
def test():
    """Get Value from a page."""
    value = request.form['value']
    type = request.form['id']

    msg = sendValue(dic[type], value)
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.connect(server)
    print(msg)
    print(codecs.encode(msg))
    client.send(codecs.encode(msg))
    print(client.recv(4096))
    client.close()
    return '', 204

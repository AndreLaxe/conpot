function hide(obj, num_slider) {
    var el = document.getElementById(obj);
    if (el.style.display == 'none') {
        el.style.display = 'block';
    }
    else {
        if (confirm("Modification ?")) {
            var password = prompt("password for admin", "password");
            if (password == "admin") {
                var val = num_slider.roundSlider("getValue");
                confirm(val);
                /* Ajax return value to /test url */
                $.ajax({
                    url: '/test',
                    type: 'POST',
                    data: {
                        value: val,
                        id: obj
                    }
                })

                el.style.display = 'none';
            }
            else {

                alert("Wrong password");
                el.style.display = 'none';
            }
        }
        el.style.display = 'none';

    }
}

function getValue(obj) {
    var isChecked = document.getElementById(obj).checked;
    var the_value = isChecked ? 1 : 0;
    $.ajax({
        url: '/test',
        type: 'POST',
        data: {
            value: the_value,
            id: obj
        }
    })
}
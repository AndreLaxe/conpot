import crcmod
import codecs
import random
from math import floor, ceil, modf

crc16 = crcmod.mkCrcFun(0x13D65, 0xFFFF, True, 0xFFFF) # Checksum for DNP3

response = ''
fm = '1'
lm = '1'
confirm = '0'
unsolicited = '0'
sequence1 = random.randint(0,37)
sequence2 = random.randint(0,15)

def checksum(data):
    global response
    crc = hex(crc16(codecs.decode(data,'hex')))[2:]
    if len(crc) < 4:
        response += '{0:02x}'.format(int(crc[2:],16)) + '{0:02x}'.format(int(crc[:1],16)) 
    else:
        response += '{0:02x}'.format(int(crc[2:],16)) + '{0:02x}'.format(int(crc[:2],16))

def build_response(data):
    """
    Return Header + Data respond
    """
    global response
    response += '0564' #start bytes
    response += '{0:02x}'.format(len(data)//2 + 5) #length data
    response += '44' #control 
    response += '03000400' #source 3 to destionation 4
    checksum(bytes(response,'utf8'))
    for i in range(ceil((len(data)//2)/16)):
        data_chunk = data[2*16*i : 2*16*(i+1)]
        response += data_chunk
        checksum(data_chunk)

def build_layer_application():
    global  sequence1, sequence2
    a = fm + lm + bin(sequence1)[2:].zfill(6) #transport control
    sequence1 = (sequence1 + 1)%37 #sequence
    data = hex(int(a[:4],2))[2:] + hex(int(a[4:],2))[2:]
    data += hex(int(fm + lm + confirm + unsolicited,2))[2:]
    data += hex(int(sequence2))[2:] #application control
    sequence2 = (sequence2 + 1)%37
    return data


def sendValue(id,value):
    data = build_layer_application()
    data += '02'
    data += '1e0300'
    data += '{0:02x}'.format(id) + '{0:02x}'.format(id)
    a = format(int(value), '#010x')[2:]
    for i in range(len(a)//2):
        data += a[-2 - 2*i: len(a) - 2*i]
    global response
    response = ""
    build_response(data)
    return response
    

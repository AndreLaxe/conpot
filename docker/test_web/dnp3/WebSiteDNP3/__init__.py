"""
The flask application package.
"""

from flask import Flask
app = Flask(__name__)

import WebSiteDNP3.views

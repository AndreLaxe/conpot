#!/bin/sh

TEMPLATE="dnp3"

#Ok, le startup sert à configurer mysql et le mettre en place (il doit
#être lancer avec root) et le script.sh est mon point d'entrée (à lancer
#non root)

if [ -d /app/mysql ]; then
	echo "[i] MySQL directory already present, skipping creation"
else
	echo "[i] MySQL data directory not found, creating initial DBs"
	
	mysql_install_db --user=root > /dev/null

	if [ "$MYSQL_ROOT_PASSWORD" = "" ]; then
		MYSQL_ROOT_PASSWORD=111111
		echo "[i] MySQL root Password: $MYSQL_ROOT_PASSWORD"
	fi

	MYSQL_DATABASE=${MYSQL_DATABASE:-""}
	MYSQL_USER=${MYSQL_USER:-""}
	MYSQL_PASSWORD=${MYSQL_PASSWORD:-""}

	if [ ! -d "/run/mysqld" ]; then
		mkdir -p /run/mysqld
	fi
	
	tfile=`mktemp`
	if [ ! -f "$tfile" ]; then
		return 1
	fi

	cat << EOF > $tfile
USE mysql;
FLUSH PRIVILEGES;
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY "$MYSQL_ROOT_PASSWORD" WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost' WITH GRANT OPTION;
UPDATE user SET password=PASSWORD("") WHERE user='root' AND host='localhost';
EOF

	if [ "$MYSQL_DATABASE" != "" ]; then
		echo "[i] Creating database: $MYSQL_DATABASE"
		echo "CREATE DATABASE IF NOT EXISTS \`$MYSQL_DATABASE\` CHARACTER SET utf8 COLLATE utf8_general_ci;" >> $tfile

		if [ "$MYSQL_USER" != "" ]; then
			echo "[i] Creating user: $MYSQL_USER with password $MYSQL_PASSWORD"
			echo "GRANT ALL ON \`$MYSQL_DATABASE\`.* to '$MYSQL_USER'@'%' IDENTIFIED BY '$MYSQL_PASSWORD';" >> $tfile
		fi
	fi

	/usr/bin/mysqld --user=root --bootstrap --verbose=0  & < $tfile
	rm -f $tfile
fi

exec /usr/bin/mysqld --user=root --console &

if [ "$TEMPLATE" == "guardian_ast" ]; then
	exec su-exec 1000:1000  python3 /test_web/guardian/runserver.py &
fi

if [ "$TEMPLATE" == "dnp3" ]; then
	exec su-exec 1000:1000  python3 /test_web/dnp3/runserver.py &
fi

exec su-exec 1000:1000 /home/conpot/.local/bin/conpot --template $TEMPLATE --logfile /var/log/conpot/conpot.log -f --temp_dir /tmp



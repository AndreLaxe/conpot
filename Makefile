.PHONY: docker
build-docker:
	docker build --no-cache -t conpot docker/

run-docker: build-docker
	docker run -p 20000:20000 -p 80:8800 -p 102:10201 -p 502:5020 -p 161:16100/udp -p 47808:47808/udp -p 623:6230/udp -p 21:2121 -p 69:6969/udp -p 44818:44818 -p 10001:10001 -v /home/honeypot/workspace/conpot/docker/test_web:/test_web --network=bridge conpot

clean-docker:
	sudo docker system prune --all --force --volumes

import struct
import socket
import time
import logging
import sys
import codecs
from datetime import datetime
from lxml import etree
from gevent.server import StreamServer
import mysql.connector as mysql
logger = logging.getLogger(__name__)

import gevent
import conpot.core as conpot_core
from gevent.server import StreamServer
from conpot.core.protocol_wrapper import conpot_protocol
from conpot.protocols.dnp3.decoder import Decoder
from conpot.protocols.dnp3.response import RESPONSE

@conpot_protocol
class DNP3Server(object):
    
    def __init__(self,template,template_directory,args):
        self.server = None
        self.databus = conpot_core.get_databus()
        list =  self.databus.get_value('list').split(", ")
        t1 = int(time.mktime(datetime.now().timetuple()))
        try:
            # get values from xml files
            self.cnx = mysql.connect(database="test")
            self.cursor = self.cnx.cursor()
            # create table for information
            self.cursor.execute("CREATE TABLE info (name TEXT, value TEXT)")
            self.cursor.execute("INSERT INTO info VALUES (%s,%s)", ("adsress", self.databus.get_value("address")))
            self.cnx.commit()
            self.cursor.execute("INSERT INTO info VALUES(%s,%s)", ("phone", self.databus.get_value("phone")))
            self.cnx.commit()
            # create table for values
            self.cursor.execute("CREATE TABLE station (id INTEGER, name TEXT, value INTEGER)")
            index = 0
            for element in  list:
                self.cursor.execute("INSERT INTO station VALUES (%s,%s,%s)", (index, element, self.databus.get_value(element)))
                self.cnx.commit()
                index += 1
            self.cursor.execute("INSERT INTO station VALUES (%s,%s,%s)", (30, 'time', t1))
            self.cnx.commit()
        except Exception as e:
            logger.info("Wrong \n")
            logger.info(str(e))
        logger.info("DNP3 Server Initialized")

    def handle(self,sock, addr):
        session = conpot_core.get_session('dnp3',
                                           addr[0],
                                           addr[1],
                                           sock.getsockname()[0],
                                           sock.getsockname()[1])
        logger.info('New DNP3 connection from %s:%d. (%s)', addr[0], addr[1], session.id)
        session.add_event({'type': 'NEW_CONNECTION'})

        # Return values
        def get_value(self):
            values = []
            self.cursor.execute("SELECT value FROM station WHERE id != 30")
            a = self.cursor.fetchall()
            for i in a:
                values.append(i[0])
            return values
        
        # return time
        def get_time(self):
            self.cursor.execute("SELECT value FROM station WHERE id == 30")
            return self.cursor.fetchall()[0]

        # Write value at index id
        def set_value(self,id,hex):
            value = ''
            for i in (range(len(hex)//2)):
                value +=  hex[-2 - 2*i: len(hex) - 2*i]
            self.cursor.execute("UPDATE station SET value = %s WHERE id = %s", (int(value,16), id))
            self.cnx.commit()

        def receive(self):
            while True:
                try:
                    # Get the initial data
                    request = sock.recv(4096)
                    # The connection has been closed
                    if not request:
                        break
                    while not (b'\n' in request or b'00' in request):
                        request += sock.recv(4096)
                    logger.info("Received : %s", request)
                    try:
                        request = codecs.decode(request,'hex').hex()
                        request = codecs.decode(request, 'hex')
                    except Exception as e:
                        logger.info(str(e))
                        break
                    D = Decoder()
                    if ((D.main(request))):
                        logger.info("Header[0] : %s", D.header[0])
                        logger.info('Start bytes : %s', codecs.decode('0564','hex'))
                        if (D.header[0] != codecs.decode('0564','hex')):
                            logger.info("Non DNP3 receive %s:%s. (%s)", addr[0], addr[1], session.id)
                            break
                        R = RESPONSE()
                    else:
                        logger.info("Non DNP3 protocol receive %s:%s. (%s)", addr[0], addr[1], session.id)
                        break
                    
                    #Check which case is it
                    if D.function == 'WRITE' and D.request[0] == 'TIME AND DATE':
                        data = R.respond_write_time_date()
                        R.build_response(data)
                        send = R.response
                    elif D.function =='WRITE' and D.request[0] == '32-BIT ANALOGUE INPUT WITHOUT FLAG':
                        for i in range(D.start, D.stop+1):
                            set_value(self,i,D.data.hex())
                        data = R.respond_write()
                        R.build_response(data)
                        send = R.response
                    elif D.function == "ENABLE_UNSOLICITED" or D.function == "DISABLE_UNSOLICITED":
                        data = R.respond_message()
                        R.build_response(data)
                        send = R.response
                    elif D.function == "COLD_RESTART":
                        data = R.respond_cold_restart()
                        R.build_response(data)
                        send = R.response
                    elif D.function == "WARM_RESTART":
                        data = R.respond_warm_restart()
                        R.build_response(data)
                        send = R.response
                    elif D.function == "READ":
                        if D.request[0] == 'TIME AND DATE':
                            time = get_time(self)
                            data = R.respond_read_time_date(time)
                            R.build_response(data)
                            send = R.response
                        if 'CLASS 0 DATA' in D.request:
                            values = get_value(self)
                            data = R.respond_read(D,values)
                            logger.info(data)
                            R.build_response(data)
                            send = R.response
                    else:
                        #No case so unknow function
                        data = R.respond_unknow_function()
                        R.build_response(data)
                        send = R.response
                    
                    #Begin to send response
                    if send:
                        sock.send(codecs.encode(send))
                        # sock.send(str_to_bytes(response))
                    session.add_event({"type": "AST {0}".format(D.function), "request": D.request, "response": send})
                except Exception as e:
                    logger.exception(('Unknown Error: {}'.format(str(e))))

            logger.info('DNP3 client disconnected %s:%d. (%s)', addr[0], addr[1], session.id)
            session.add_event({'type': 'CONNECTION_LOST'})
        
        receive(self)

    def start(self, host, port):
        self.host = host
        self.port = port
        connection = (host, port)
        self.server = StreamServer(connection, self.handle)
        logger.info('DNP3 server started on: {0}'.format(connection))
        self.server.serve_forever()

    def stop(self):
        self.server.stop()


if __name__ == '__main__':
    # Set vars for connection information
    TCP_IP = '127.0.0.1'
    TCP_PORT = 20000
    import os
    dir_name = os.path.dirname(conpot.__file__)
    server = DNP3Server(None, None, None)
    server.databus.initialize(dir_name + '/templates/dnp3/template.xml')
    try:
        server.start(TCP_IP, TCP_PORT)
    except KeyboardInterrupt:
        server.stop()
        
        

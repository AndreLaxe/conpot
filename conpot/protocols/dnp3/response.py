import socket
import crcmod
from collections import namedtuple
import struct
from math import floor, ceil, modf
import codecs
import random

OBJ_VAR_VALUE = {
    (1, 1): 'SINGLE BINARY BIT INPUT',  # The single bit binary input object is used to represent the state of a digital input point (hardware or software).
    (1, 2): 'BINARY INPUT WITH STATUS',
    (2, 1): 'BINARY INPUT WITHOUT TIME',
    (2, 2): 'BINARY INPUT WITH TIME',
    (10, 1): 'BINARY OUTPUT',
    (10, 2): 'BINARY OUTPUT WITH STATUS',
    (12, 1): 'CONTROL RELAY OUTPUT BLOCK',
    (20, 5): '32-BIT BINARY COUNTER WITHOUT FLAG',
    (22, 1): '32-BIT COUNTER CHANGE EVENT WITHOUT FLAG',
    (30, 3): '32-BIT ANALOGUE INPUT WITHOUT FLAG',
    (32, 1): '32-BIT ANALOGUE EVENT WITHOUT TIME',
    (40, 1): '32-BIT ANALOGUE OUTPUT STATUS',
    (41, 1): '32-BIT ANALOGUE OUTPUT BLOCK',
    (50, 1): 'TIME AND DATE',
    (52, 1): 'TIME DELAY COARSE',
    (52, 2): 'TIME DELAY FINE',
    (60, 1): 'CLASS 0 DATA',
    (60, 2): 'CLASS 1 DATA',
    (60, 3): 'CLASS 2 DATA',
    (60, 4): 'CLASS 3 DATA',
    (80, 1): 'INTERNAL INDICATIONS',
    (90, 1): 'APPLICATION IDENTIFIER',
    (150, 1): 'BYTE SEQUENCE (STRING)',
    (151, 1): 'SPA MESSAGE'
}

crc16 = crcmod.mkCrcFun(0x13D65, 0xFFFF, True, 0xFFFF) # Checksum for DNP3

class RESPONSE(object):
    def __init__(self):
        self.response = ''
        self.fm = '1'
        self.lm = '1'
        self.confirm = '0'
        self.unsolicited = '0'
        self.sequence1 = random.randint(0,37)
        self.sequence2 = random.randint(0,15)

    def checksum(self,data):
        crc = hex(crc16(codecs.decode(data,'hex')))[2:]
        if len(crc) < 4:
            self.response += '{0:02x}'.format(int(crc[2:],16)) + '{0:02x}'.format(int(crc[:1],16)) 
        else:
            self.response += '{0:02x}'.format(int(crc[2:],16)) + '{0:02x}'.format(int(crc[:2],16))

    def build_response(self,data):
        """
        Return Header + Data respond
        """
        self.response += '0564' #start bytes
        self.response += '{0:02x}'.format(len(data)//2 + 5) #length data
        self.response += '44' #control 
        self.response += '03000400' #source 3 to destionation 4
        self.checksum(bytes(self.response,'utf8'))
        for i in range(ceil((len(data)//2)/16)):
            data_chunk = data[2*16*i : 2*16*(i+1)]
            self.response += data_chunk
            self.checksum(data_chunk)

    def build_layer_application(self):
        a = self.fm + self.lm + bin(self.sequence1)[2:].zfill(6) #transport control
        self.sequence1 = (self.sequence1 + 1)%37 #sequence
        data = hex(int(a[:4],2))[2:] + hex(int(a[4:],2))[2:]
        data += hex(int(self.fm + self.lm + self.confirm + self.unsolicited,2))[2:]
        data += hex(int(self.sequence2))[2:] #application control
        self.sequence2 = (self.sequence2 + 1)%37
        return data

    def respond_read(self,msg,values):
        """
        Return Application Layer to read function
        """
        data = self.build_layer_application()
        data += '81'
        data += '0000'
        if 'CLASS 0 DATA' in msg.request:
            data += '1e0300' #hex code Class 0 + Qualifier Field
            stop = len(values) - 1
            data += '00' + '{0:02x}'.format(stop) #Start and Stop
            for i in range(len(values)):
                value = ''
                a = format(values[i], '#010x')[2:]
                for i in (range(len(a)//2)):
                    value +=  a[-2 - 2*i: len(a) - 2*i]
                data += value #Hex Value or '{0:08x}'.format(value)
        return data

    def respond_write(self):
        """
        Return Application Layer to write function
        """
        data = self.build_layer_application()
        data += '81'
        data += '0000'
        return data

    def respond_write_time_date(self):
        """
        Return Application Layer to write time and date function
        """
        data = self.build_layer_application()
        data += '81' #function_code
        data += '0000' #INN
        return data

    def respond_read_time_date(self,time):
        """
        Return Application Layer to read time and date function
        """
        data = self.build_layer_application()
        data += '81' #function_code
        data += '0000' #INN
        data += hex(time)
        return data
    
    def respond_message(self):
        data = self.build_layer_application()
        data += '81' #function_code
        data += '8000' #restart device
        return data

    def respond_cold_restart(self):
        """
        Return Application Layer to cold restart function
        """
        data = self.build_layer_application()
        data += '81' #function_code
        data += '0000' #INN
        data += '340207018813' # time delay 500ms
        return data
    
    def respond_warm_restart(self):
        """
        Return Application Layer to warm restart function
        """
        data = self.build_layer_application()
        data += '81' #function_code
        data += '0000' #INN
        data += '34020701d007' # time delay 200ms
        return data

    def respond_unknow_function(self):
        """
        Return Application Layer to unknow function
        """
        data = self.build_layer_application()
        data += '81' #function_code
        data += '1001' #INN time sync and no function
        return data


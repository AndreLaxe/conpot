import socket
import crcmod
from collections import namedtuple
import struct
from math import ceil, modf
import codecs
import logging

logger = logging.getLogger(__name__)


PRIM_FUNC_CODES = {
    0: "Reset of remote link",
    1: "Reset of user process ",
    2: "Test function for link",
    3: "User data",
    4: "Unconfirmed user data",
    9: "Request link status"
}


SEC_FUNC_CODES = {
    0: "ACK - positive acknowledgment",
    1: "NACK - Message not accepted, link busy",
    11: "Status of link (DFC = 0 or 1)",
    14: "Link service not functioning",
    15: "Link service not used or implemented"
}

OCTET_SIZE = {
    0: 0,
    1: 1,
    2: 2,
    3: 4
}

applicationFunctionCode = {
    0: "CONFIRM",
    1: "READ",
    2: "WRITE",
    3: "SELECT",
    4: "OPERATE",
    5: "DIRECT_OPERATE",
    6: "DIRECT_OPERATE_NR",
    7: "IMMED_FREEZE",
    8: "IMMED_FREEZE_NR",
    9: "FREEZE_CLEAR",
    10: "FREEZE_CLEAR_NR",
    11: "FREEZE_AT_TIME",
    12: "FREEZE_AT_TIME_NR",
    13: "COLD_RESTART",
    14: "WARM_RESTART",
    15: "INITIALIZE_DATA",
    16: "INITIALIZE_APPL",
    17: "START_APPL",
    18: "STOP_APPL",
    19: "SAVE_CONFIG",
    20: "ENABLE_UNSOLICITED",
    21: "DISABLE_UNSOLICITED",
    22: "ASSIGN_CLASS",
    23: "DELAY_MEASURE",
    24: "RECORD_CURRENT_TIME",
    25: "OPEN_FILE",
    26: "CLOSE_FILE",
    27: "DELETE_FILE",
    28: "GET_FILE_INFO",
    29: "AUTHENTICATE_FILE",
    30: "ABORT_FILE",
    31: "ACTIVATE_CONFIG",
    32: "AUTHENTICATE_REQ",
    33: "AUTH_REQ_NO_ACK",
    129: "RESPONSE",
    130: "UNSOLICITED_RESPONSE",
    131: "AUTHENTICATE_RESP",
}

OBJ_VAR_VALUE = {
    (1, 1): 'SINGLE BINARY BIT INPUT',  # The single bit binary input object is used to represent the state of a digital input point (hardware or software).
    (1, 2): 'BINARY INPUT WITH STATUS',
    (2, 0): 'BINARY INPUT CHANGE DEFAULT VARIATION',
    (2, 1): 'BINARY INPUT WITHOUT TIME',
    (2, 2): 'BINARY INPUT WITH TIME',
    (10, 1): 'BINARY OUTPUT',
    (10, 2): 'BINARY OUTPUT WITH STATUS',
    (12, 1): 'CONTROL RELAY OUTPUT BLOCK',
    (20, 5): '32-BIT BINARY COUNTER WITHOUT FLAG',
    (22, 1): '32-BIT COUNTER CHANGE EVENT WITHOUT FLAG',
    (30, 3): '32-BIT ANALOGUE INPUT WITHOUT FLAG',
    (32, 1): '32-BIT ANALOGUE EVENT WITHOUT TIME',
    (40, 1): '32-BIT ANALOGUE OUTPUT STATUS',
    (41, 1): '32-BIT ANALOGUE OUTPUT BLOCK',
    (50, 1): 'TIME AND DATE',
    (52, 1): 'TIME DELAY COARSE',
    (52, 2): 'TIME DELAY FINE',
    (60, 1): 'CLASS 0 DATA',
    (60, 2): 'CLASS 1 DATA',
    (60, 3): 'CLASS 2 DATA',
    (60, 4): 'CLASS 3 DATA',
    (80, 1): 'INTERNAL INDICATIONS',
    (90, 1): 'APPLICATION IDENTIFIER',
    (150, 1): 'BYTE SEQUENCE (STRING)',
    (151, 1): 'SPA MESSAGE'
}

crc16 = crcmod.mkCrcFun(0x13D65, 0xFFFF, True, 0xFFFF)

class Decoder():
    """
    Decode any DNP3 packets to a header and data part

    Then parse the packeets to set informations
    """

    def __init__(self):
        self.header = ''
        self.control = ''
        self.transport_control = ''
        self.application_control = ''
        self.function = ''
        self.request = []
        self.obj = []
        self.var = []
        self.qualifier = []
        self.start = ''
        self.stop = ''
        self.INN = ''
        self.data = ''

#################################################################
#                                                               #
#           Function to unpack header and data reminder         #
#                                                               #
#################################################################

    def parse_header(self, packet_header):
        """
        Parse header to return a tuple with header's elements
        """

        Header = namedtuple('unpack_header',
                            """start_bytes, length, control, dest_addr,
                            source_addr, crc""")
        # 2s = 2 char[] bytes; b = signed char integer; H = unsig short integer
        packet_header = struct.unpack('2sbsHH2s', packet_header)
        header = Header(start_bytes=packet_header[0], length=packet_header[1],
                        control=packet_header[2], dest_addr=packet_header[3],
                        source_addr=packet_header[4], crc=packet_header[5])

        return header

    def checksum_data(self, bloc):
        """
        Checking crc for each bloc
        """

        if crc16(bloc[:-2]) == int.from_bytes(bloc[-2:], "little"):
            return True
        return False

    def parse_data(self, packet_data):
        """
        Parse data to return the control_transport and a data string
        """

        blocs = b''
        size = len(packet_data)
        (decimal, entier) = modf(size/18)
        for i in range(0, int(entier) + ceil(decimal)):
            bloc = packet_data[18*i:18+18*i]
            if self.checksum_data(bloc):
                blocs += (bloc[:-2])
            else:
                return 0

        return blocs

    def header_control(self, control):
        """
        Return tuple with DIR, PRM, FCB, FCV and control function code
        """

        bits = "0x{:02x}".format(int.from_bytes(control, "big"))[2:]
        bit = bin(int(bits[0], 16))[2:].zfill(4)
        return (int(bit[0], 16), int(bit[1], 16), int(bit[2], 16),
                int(bit[3], 16), int(bits[1], 16))

    def unpack1(self, packet):
        """
        Unpack header packet and return data string and header tuple
        """

        packet_header, packet_data = packet[:10], packet[10:]
        self.header = self.parse_header(packet_header)
        self.data = self.parse_data(packet_data)
        self.control = self.header_control(self.header.control)
        if len(self.data) == self.header.length - 5:
            logger.info("Packet fully set")
        else:
            logger.info("Packet corrupted !")

#################################################################
#                                                               #
#           Function to unpack header and data reminder         #
#                                                               #
#################################################################

    def control_trans(self, control):
        """
        Set transport control with tuple Final, First, Sequence
        """

        bits = str("0x{:02x}".format(control))[2:]
        bit1 = bin(int(bits[0], 16))[2:].zfill(4)
        bit2 = bin(int(bits[1], 16))[2:].zfill(4)
        self.transport_control = (int(bit1[0], 2), int(bit1[1], 2), int(bit1[2:] + bit2, 2))

    def control_app(self, control):
        """
        Set application control with tuple
        First, Final, Confirm, Unsolicited, Sequence
        """

        bits = str("0x{:02x}".format(control))[2:]
        bit = bin(int(bits[0], 16))[2:].zfill(4)
        self.application_control = (int(bit[0], 16), int(bit[1], 16),
                                    int(bit[2], 16), int(bit[3], 16),
                                    int(bits[1], 16))

    def field(self, range):
        """
        Set qualifier with a tuple Prefix, Range
        """

        bits = str("0x{:02x}".format(range))[2:]
        self.qualifier.append((int(bits[0], 16), int(bits[1], 16)))

    def Range(self, qualifier, data):

        size, qual = qualifier
        if not (0 <= size <= 3) or (qual == 10 or qual >= 12):
            logger.info("Size not valid, reserved !")
            return 0
        octet = OCTET_SIZE[size]
        if (qual == 0 or qual == 3):  # 8-bits start and stop Range or address
            if(size == 0):
                start = data[0]
                stop = data[1]
                return start, stop, data[2:]
            return 0
        elif (qual == 1 or qual == 4):  # 16-bits start and stop Range or address
            if(size == 0):
                start = data[0:2]
                stop = data[2:4]
                return start, stop, data[4:]
            return 0
        elif (qual == 2 or qual == 5):  # 32-bits start and stop Range or address
            if(size == 0):
                start = data[0:4]
                stop = data[4:8]
                return start, stop, data[8:]
            return 0
        elif qual == 6:
            return 0, 0, data
        elif qual == 7:  # 8-bits field quantity
            quantity = data[0]
            index = [data[1 + i*octet:i*octet + octet + 1] for i in
                     range(0, quantity)]
            data = data[quantity*octet + 1:]
        elif qual == 8:  # 16-bits field quantity
            quantity = data[0:2]
            index = [data[2 + i*octet:i*octet + octet + 2] for i in
                     range(0, quantity)]
            data = data[quantity*octet + 2:]
        elif qual == 9:  # 32-bits field quantity
            quantity = data[0:4]
            index = [data[4 + i*octet:i*octet + octet + 4] for i in
                     range(0, quantity)]
            data = data[quantity*octet + 4:]
        return quantity, index, data

    def OQR(self, request,index):
        """
        Set Obj, Var, Request for a Data Object
        """

        self.obj.append(request[0])
        self.var.append(request[1])
        self.field(request[2])
        self.request.append(OBJ_VAR_VALUE[(request[0], request[1])])
        (self.start, self.stop, d) = self.Range(self.qualifier[index], request[3:])
        data = d
        return data

    def unpack2(self, data):
        """
        Unpack data part end deal with Data Object
        """

        self.control_trans(data[0])
        self.control_app(data[1])
        self.function = applicationFunctionCode[data[2]]
        if self.function == 'WRITE':
            self.data = self.OQR(data[3:],0)
            return 1

        elif ((self.function == 'RESPONSE') or
        (self.function == 'UNSOLICITED RESPPONSE')):
            self.INN = data[3:5]
            request = data[5:]
            self.data = self.OQR(request,0)
            return 1

        elif self.function == 'CONFIRM':
            self.data = ''
            return 1

        elif self.function == 'STOP_APPL' or self.function == 'START_APPL':
            self.data = ''
            return 1

        elif self.function == 'READ' or 'DISABLE' in self.function or 'ENABLE' in self.function:
            self.data = data[3:]
            i = 0
            while len(self.data) != 0:  # chunk each data object
                self.data = self.OQR(self.data,i)
                i += 1
            return 1


    def main(self, paquet):
        try:
            self.unpack1(paquet)
            self.unpack2(self.data)
            return 1
        except Exception as e:
            logger.info(str(e))
            logger.info("Something wrong")
            return 0
# Copyright (C) 2015  Lukas Rist <glaslos@gmail.com>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

"""
Service support based on gaspot.py [https://github.com/sjhilt/GasPot]
Original authors: Kyle Wilhoit and Stephen Hilt
"""

from gevent.server import StreamServer
import datetime
import random
import conpot
import codecs
import conpot.core as conpot_core
from conpot.core.protocol_wrapper import conpot_protocol
from conpot.helpers import str_to_bytes
import threading
from time import sleep
from numpy.random import geometric
import mysql.connector as mysql

import logging
logger = logging.getLogger(__name__)

# 9999 indicates that the command was not understood and
# FF1B is the checksum for the 9999
AST_ERROR = "9999FF1B\n"


@conpot_protocol
class GuardianASTServer(object):
    def __init__(self, template, template_directory, args):
        self.server = None
        self.databus = conpot_core.get_databus()
        # dom = etree.parse(template)
        self.fill_offset_time = datetime.datetime.utcnow()
        self.list = {}
        self.numbers = self.databus.get_value('numbers')
        # Set values of stations
        for i in range(self.numbers):
            try:
                self.list[i]= {
                    'name': self.databus.get_value('product' + str(i+1)),
                    'volume': self.databus.get_value('vol' + str(i+1)),
                    'volumetc': random.randint(self.databus.get_value('vol' + str(i+1)), self.databus.get_value('vol' + str(i+1)) + 200),
                    'ullage': str(self.databus.get_value('ullage' + str(i+1))),
                    'height': str(self.databus.get_value('height' + str(i+1))).ljust(5, '0'),
                    'h2o': str(self.databus.get_value('h2o' + str(i+1))).ljust(4, '0'),
                    'temp': str(self.databus.get_value('temp' + str(i+1))).ljust(5, '0'),
                    'price': str(self.databus.get_value('price' + str(i+1)))
                    }
            except Exception as e:
                logger.info(str(e))
        self.currency = self.databus.get_value('currency')
        self.station = self.databus.get_value('Name')
        try:
            self.cnx = mysql.connect(database='test')
            self.cursor = self.cnx.cursor()
            
            # Commit Values
            self.cursor.execute("CREATE TABLE station (id INTEGER, name TEXT, volume DOUBLE, price TEXT)")
            for i in range(self.numbers):
                self.cursor.execute("INSERT INTO station VALUES (%s,%s,%s,%s)",
                                    (i,
                                    self.list[i]['name'],
                                    self.list[i]['volume'],
                                    self.list[i]['price']+self.currency))
                self.cnx.commit()

            # Commit Profil
            self.cursor.execute("CREATE TABLE profil (name TEXT, address TEXT, phone TEXT)")
            self.cursor.execute("INSERT INTO profil VALUES (%s,%s,%s)", 
                                (self.databus.get_value("Name"),
                                self.databus.get_value("Address"),
                                self.databus.get_value("Phone"))
                                )
            self.cnx.commit()
        except Exception as e:
           logger.info(str(e))
        self.stop_th = [False for i in range(self.numbers)]
        logger.info('Conpot GuardianAST initialized')

    def handle(self, sock, addr):
        session = conpot_core.get_session('guardian_ast',
                                           addr[0],
                                           addr[1],
                                           sock.getsockname()[0],
                                           sock.getsockname()[1])
        logger.info('New GuardianAST connection from %s:%d. (%s)', addr[0], addr[1], session.id)
        session.add_event({'type': 'NEW_CONNECTION'})
        current_time = datetime.datetime.utcnow()
        fill_start = self.fill_offset_time - datetime.timedelta(minutes=313)
        fill_stop = self.fill_offset_time - datetime.timedelta(minutes=303)

        def I20100():
            ret = '\nI20100\n' + str(current_time.strftime('%m/%d/%Y %H:%M'))
            ret += '\n\n' + self.station + '\n\n\n\nIN-TANK INVENTORY\n\n'
            ret += '\t' + 'TANK'.ljust(7) + 'PRODUCT'.ljust(10) + 'VOLUME TC'.ljust(13) + 'VOLUME'.ljust(9) + 'ULLAGE'.ljust(9) + 'HEIGHT'.ljust(9) + 'WATER'.ljust(9) + 'TEMP'.ljust(9) + 'PRICE(' + self.currency +')'
            for i in range(self.numbers):
                ret += '\n\t' + str(i+1).ljust(7) + self.list[i]['name'].ljust(10) + str(self.list[i]['volume']).ljust(13) + str(self.list[i]['volumetc']).ljust(9) + self.list[i]['ullage'].ljust(9) + self.list[i]['height'].ljust(9) + self.list[i]['h2o'].ljust(9) + self.list[i]['temp'].ljust(9) + self.list[i]['price']
            ret += '\n'
            return ret

        ###########################################################################
        #
        # Only one Tank is listed currently in the I20200 command
        #
        ###########################################################################
        def I20200():
            ret = '\nI20200\n' + str(current_time.strftime('%m/%d/%Y %H:%M'))
            ret += '\n\n' + self.station + '\n\n\n\nDELIVERY REPORT\n\n'
            ret += 'T 1:' + self.list[0]['name'] + '\nINCREASE   DATE / TIME             '+ self.currency + ' TC ' + self.currency +' WATER  TEMP DEG F  HEIGHT\n\n'

            ret += '      END: ' + str(fill_stop.strftime('%m/%d/%Y %H:%M')) + '         ' + str(self.list[0]['volume'] + 300) + '       ' + str(self.list[0]['volumetc'] + 300) + '   ' + self.list[0]['h2o'] + '      ' + self.list[0]['temp'] + '    ' + self.list[0]['height'] + '\n'
            ret += '    START: ' + str(fill_start.strftime('%m/%d/%Y %H:%M')) + '         ' + str(self.list[0]['volume'] - 300) + '       ' + str(self.list[0]['volumetc'] - 300) + '   ' + self.list[0]['h2o'] + '      ' + self.list[0]['temp'] + '    ' + str(float(self.list[0]['height']) - 23) + '\n'
            ret += '   AMOUNT:                          ' + str(self.list[0]['volume']) + '       ' + str(self.list[0]['volumetc']) + '\n\n'
            return ret

        ###########################################################################
        #
        # I20300 In-Tank Leak Detect Report
        #
        ###########################################################################
        def I20300():
            ret = '\nI20300\n' + str(current_time.strftime('%m/%d/%Y %H:%M'))
            ret += '\n\n' + self.station + '\n\n\n'
            for i in range(self.numbers):
                ret += 'TANK ' + str(i+1) + '    ' + self.list[i]['name'] + '\n    TEST STATUS: OFF\nLEAK DATA NOT AVAILABLE ON THIS TANK\n\n'
            return ret

        ###########################################################################
        # Shift report command I20400 only one item in report at this time,
        # but can always add more if needed
        ###########################################################################
        def I20400():
            ret = '\nI20400\n' + str(current_time.strftime('%m/%d/%Y %H:%M'))
            ret += '\n\n' + self.station + '\n\n\n\nSHIFT REPORT\n\n'
            ret += 'SHIFT 1 TIME: 12:00 AM\n\nTANK PRODUCT\n\n'
            ret += '  1  ' + self.list[0]['name'] + ' VOLUME TC VOLUME  ULLAGE  HEIGHT  WATER   TEMP\n'
            ret += 'SHIFT  1 STARTING VALUES      ' + str(self.list[0]['volume']) + '     ' + str(self.list[0]['volumetc']) + '    ' + self.list[0]['ullage'] + '   ' + self.list[0]['height'] + '   ' + self.list[0]['h2o'] + '    ' + self.list[0]['temp'] + '\n'
            ret += '         ENDING VALUES        ' + str(self.list[0]['volume'] + 940) + '     ' + str(self.list[0]['volumetc'] + 886) + '    ' + str(int(self.list[0]['ullage']) + 345) + '   ' + str(float(self.list[0]['height']) + 53) + '  ' + self.list[0]['h2o'] + '    ' + self.list[0]['temp'] + '\n'
            ret += '         DELIVERY VALUE          0\n'
            ret += '         TOTALS                940\n\n'
            return ret

        ###########################################################################
        # I20500 In-Tank Status Report
        ###########################################################################
        def I20500():
            ret = '\nI20500\n' + str(current_time.strftime('%m/%d/%Y %H:%M'))
            ret += '\n\n\n' + self.station + '\n\n\n'
            ret += 'TANK   PRODUCT                 STATUS\n\n'
            choice = ['  NORMAL\n\n', '  HIGH WATER ALARM\n']
            for i in range(self.numbers):
                ret += '  ' + str(i) + '    ' + self.list[i]['name'] + choice[random.randint(0,1)]
            return ret

         ###########################################################################
        # Change price pump
        ###########################################################################

        def I2060(price,id):
            self.list[id]['price'] = '{:0.3f}'.format(price)
            self.cursor.execute("UPDATE station SET price = %s WHERE id = %s",
                               (self.list[id]['price'], id))
            self.cnx.commit()

        ###########################################################################
        # Reaload station id
        ###########################################################################

        def reaload(self,id):
            stop_p(id)
            sleep(10)
            for id in range(self.numbers):
                try:
                    self.list[id]['volume'] = self.list[id]['volumetc']
                    try:
                        self.cursor.execute("UPDATE station SET volume = %s WHERE id = %s", (self.list[id]['volume'],id))
                        self.cnx.commit()
                    except Exception as e:
                        logger.info(str(e))
                except Exception as e:
                    logger.error(str(e))
            sleep(1)
            start_p()
        
        ###########################################################################
        # Change values stations
        ###########################################################################

        def change(self, id):
            logger.info('Starting change pomp %d. (%s)', id, session.id)

            ###########################################################################
            # Change level value
            ###########################################################################

            def levl(stop_thread):
                while True:
                    sleep(0.1)
                    if not stop_thread():
                        z = geometric(p=0.20, size=100)
                        sleep(random.choice(z))
                        try:
                            if self.list[id]['volume'] > self.list[id]['volumetc']*0.10:
                                self.list[id]['volume'] = float(format(self.list[id]['volume'] - float(random.randint(500,7000)/100), '.2f'))
                                try:
                                    self.cursor.execute("UPDATE station SET volume = %s WHERE id = %s", (self.list[id]['volume'],id))
                                    self.cnx.commit()
                                except Exception as e:
                                    logger.info(str(e))
                            else:
                                if not stop_thread():
                                    logger.info('Stopping level pomp %d. (%s)', id, session.id)
                                    reaload(self,id)
                                else:
                                    break
                        except Exception as e:
                            logger.error(str(e))
                    else:
                        break
            
            ###########################################################################
            # Change temperature value
            ###########################################################################
            
            def temp(stop_thread):
                a = int(float(self.list[id]['temp'])*100)
                while True:
                    sleep(1)
                    if stop_thread():
                        logger.info('Stopping temp pomp %d. (%s)', id, session.id)
                        break
                    try:
                        self.list[id]['temp'] = str(random.randint(a-200,a+200)/100)
                    except Exception as e:
                        logger.error(str(e))
            if "levl"+str(id) not in threading.enumerate():
                globals()['levl' + str(id)] = threading.Thread(name="levl"+str(id), target=levl, args=(lambda : self.stop_th[id],))
                globals()['levl' + str(id)].start()
            if "temp"+str(id) not in threading.enumerate():
                globals()['temp' + str(id)] = threading.Thread(name="temp"+str(id), target=temp, args=(lambda : self.stop_th[id], ))
                globals()['temp' + str(id)].start()

        ###########################################################################
        # Start all threads / I40100
        ###########################################################################

        def start_p():
            self.stop_th = [False for x in range(self.numbers)]
            logger.info("Starting Station")
            for i in range(self.numbers):
                change(self,i)
            logger.info(threading.enumerate())

        ###########################################################################
        # Stop all threads / I40200
        ###########################################################################

        def stop_p(id = None):
            self.stop_th = [True for x in range(self.numbers)] #Stop thread
            logger.info("Stopping station")
            sleep(5)
            logger.info(threading.enumerate())
            try:
                for i in range(self.numbers):
                    if i != id:
                        globals()['levl'+ str(i)].join() #Kill thread
                    globals()['temp'+ str(i)].join()
            except Exception as e:
                logger.info(str(e))
            logger.info(threading.enumerate())
            logger.info("All threads kill")
            

        def receive(self):
            while True:
                try:
                    # Get the initial data
                    request = sock.recv(4096)
                    # The connection has been closed
                    if not request:
                        break
                    while not (b'\n' in request or b'00' in request):
                        request += sock.recv(4096)
                    # if first value is not ^A then do nothing
                    # thanks John(achillean) for the help
                    if request[:1] != b'\x01':
                        logger.info('Non ^A command attempt %s:%d. (%s)',
                                    addr[0], addr[1], session.id)
                        break
                    # if request is less than 6, than do nothing
                    if len(request) < 6:
                        logger.info('Invalid command attempt %s:%d. (%s)',
                                    addr[0], addr[1], session.id)
                        break

                    cmds = {"I20100": I20100, "I20200": I20200, "I20300": I20300,
                            "I20400": I20400, "I20500": I20500, "I40100": start_p,
                            "I40200": stop_p}
                    cmd = request[1:7].decode()  # strip ^A and \n out
                    response = None
                    if cmd in cmds:
                        logger.info('%s command attempt %s:%d. (%s)',
                                    cmd, addr[0], addr[1], session.id)
                        response = cmds[cmd]()
                    elif cmd.startswith("S6020"):
                        # change the tank name
                        TEMP = request.split(b'S6020')
                        if len(TEMP) < 2 and len(TEMP[1]) <= 1:
                            response = AST_ERROR
                        else:
                            value = int(chr(TEMP[1][0]))
                            TEMP1 = TEMP[1][1:].rstrip(b'\r\n').decode()
                            if 0 < value <= self.numbers:
                                if len(TEMP1) < 22:
                                    self.list[value - 1]['name'] = TEMP1.ljust(22)
                                elif len(TEMP1) > 22:
                                    self.list[value - 1]['name'] = TEMP1[:20] + "  "
                                else:
                                    self.list[value - 1]['name'] = TEMP1
                            # Follows format for S60201 for comment
                            elif value == 0:
                                if len(TEMP1) < 22:
                                    for i in range(self.numbers):
                                        self.list[i]['name'] = TEMP1.ljust(22)                                
                                elif len(TEMP1) > 22:
                                    for i in range(self.numbers):
                                        self.list[i]['name'] = TEMP1[:20] + "  " 
                                else:
                                    for i in range(self.numbers):
                                        self.list[i]['name'] = TEMP1
                            else:
                                response = AST_ERROR
                            logger.info('S6020%s: %s command attempt %s:%d. (%s)',
                                        TEMP1[0], TEMP1[1:], addr[0], addr[1],
                                        session.id)
                    # Change price for one pump
                    elif cmd.startswith("I2060"):
                        value = request.split(b'I2060')[1]
                        if len(value[1::]) != 7:
                            response = AST_ERROR
                            logger.info('I2060%s: %s command attempt %s:%s. (%s)',
                                        value, value[1::] ,addr[0], addr[1],
                                        session.id)
                        else :
                            try:
                                id = int(chr(value[0]))
                                price = float(value[1::].rstrip(b'\r\n').decode())
                                I2060(price,id)
                            except Exception as e:
                                logger.info(str(e))
                            logger.info('I2060%s: %s command attempt %s:%s. (%s)',
                                    str(id), str(price), addr[0], addr[1],
                                    session.id)
                    else:
                        response = AST_ERROR
                        # log what was entered
                        logger.info('%s command attempt %s:%d. (%s)', request, addr[0], addr[1], session.id)
                    if response:
                        sock.send(codecs.encode(response))
                        # sock.send(str_to_bytes(response))
                    session.add_event({"type": "AST {0}".format(cmd), "request": request, "response": response})
                except Exception as e:
                    logger.exception(('Unknown Error: {}'.format(str(e))))
            logger.info('GuardianAST client disconnected %s:%d. (%s)', addr[0], addr[1], session.id)
            session.add_event({'type': 'CONNECTION_LOST'})

        receive(self)

    def start(self, host, port):
        self.host = host
        self.port = port
        connection = (host, port)
        self.server = StreamServer(connection, self.handle)
        logger.info('GuardianAST server started on: {0}'.format(connection))
        self.server.serve_forever()

    def stop(self):
        self.server.stop()


if __name__ == '__main__':
    # Set vars for connection information
    TCP_IP = '127.0.0.1'
    TCP_PORT = 10001
    import os
    dir_name = os.path.dirname(conpot.__file__)
    server = GuardianASTServer(None, None, None)
    server.databus.initialize(dir_name + '/templates/guardian_ast/template.xml')
    try:
        server.start(TCP_IP, TCP_PORT)
    except KeyboardInterrupt:
        server.stop()
